from generator import samples
from tester import tests
import matplotlib.pyplot as plt
import numpy as np

generator = samples.SamplesGenerator()
# test poprawności generatora:
generator.test()


tester = tests.Tester()

for i in range(11):
    for j in range(11):
        x = 0.1*i
        y = 0.1*j
        matrix=np.array([[x, 1-x],[y, 1-y]])

        generator.set_trans_matrix(matrix)
        print(generator.get_trans_matrix())
        [o1x, o1y, o2x, o2y] = generator.generate_samples(n_samples = 2000)

        print(tester.test_on_data(o2x, o2y, k=2)) #test uczenia ann dla obiektwow aktualnych
        #print(tester.test_on_data(np.append(o1y, o2x, axis=1), o2y, k=2))
        print(tester.test_on_data(np.append(o1x, o2x, axis=1), o2y, k=2)) #test uczenia dla obiektow wraz z historią
        #print(tester.test_on_data(np.concatenate((o1y, o1x, o2x), axis=1), o2y, k=2))
