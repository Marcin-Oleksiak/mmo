from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import KFold
import numpy as np

class Tester():
    def test_on_data(self, x, y, k=2):
        score_sum = 0
        kf = KFold(n_splits=k)
        kf.get_n_splits(x)
        for train_index, test_index in kf.split(x):
            x_train, x_test = x[train_index], x[test_index]
            y_train, y_test = y[train_index], y[test_index]

            model = Sequential()
            model.add(Dense(units=10, activation='relu', input_dim=len(x[0])))
            model.add(Dense(units=10, activation='softmax'))
            model.add(Dense(units=1, activation='sigmoid'))

            model.compile(loss='binary_crossentropy', optimizer='sgd', metrics=['accuracy'])
            model.fit(x_train, y_train, epochs=5, batch_size=32, verbose=0)

            scores = model.evaluate(x_test, y_test, batch_size=32)
            score_sum += scores[1]*100

            del model
        return np.round(score_sum / k, 2)
