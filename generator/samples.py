import numpy as np
import random

random.seed(0)

class SamplesGenerator():
    trans_matrix = np.array([[0.3, 0.7],[0.6, 0.4]])
    states = np.array([2, 4])
    # print('Macierz przejść: {}'.format(trans_matrix))
    learn_set = []
    attributes = ['Clump Thickness', 'Uniformity of Cell Size', 'Uniformity of Cell Shape', 'Marginal Adhesion',
                'Single Epithelial Cell Size', 'Bare Nuclei', 'Bland Chromatin', 'Normal Nucleoli', 'Mitoses']

    drawn_objects1 = []
    drawn_objects2 = []
    class_index = 9 # domyślnie powinien być to atrybut o indeksie 9, ale jest to potwierdzane w metodzie load_data()

    def __init__(self):
        self.load_data()

    # wczytywanie zbioru uczącego z pliku z repo UCI:
    def load_data(self):
        with open('data/cancer_data.txt', 'r+') as f:
            for line in f:
                split = line.split(',')
                split[10] = split[10].replace('\n', '')
                self.learn_set.append(list(map(int, split[1:]))) # pominięcie id próbki (atrybut 0)
        self.class_index = len(self.learn_set[0]) - 1 # nr indeksu z listy atrybutów, który wskazuje klasę - ostatni

    def make_classes_01(self, y):
        y[y==2] = 0
        y[y==4] = 1

    def generate_samples(self, n_samples = 1000):
        self.drawn_objects1 = []
        self.drawn_objects2 = []
        for i in range(n_samples):
            rand = random.randint(0, len(self.learn_set) - 1)
            object1 = self.learn_set[rand]
            self.drawn_objects1.append(object1)
            state1 = object1[self.class_index] # ostatni atrybut obiektu jest właśnie klasą - stanem
            if state1 == self.states[0]:
                probabilities = self.trans_matrix[0]
            else:
                probabilities = self.trans_matrix[1]
            state2 = np.random.choice(self.states, p = probabilities)
            objects_with_class2 = [obj for obj in self.learn_set if obj[self.class_index] == state2]
            object2 = objects_with_class2[random.randint(0, len(objects_with_class2) - 1)]
            self.drawn_objects2.append(object2)

            # sprawdzenie poprawności wylosowań:
            # print('wylosowany obiekt_1: {}, jego stan (stan_1): {}'.format(object1, state1))
            # print('stan_2:{}, wylosowany obiekt_2:{}'.format(state2, object2))
            # print('-----')

        [o1x, o1y] = np.split(np.array(self.drawn_objects1), [9], axis=1)
        [o2x, o2y] = np.split(np.array(self.drawn_objects2), [9], axis=1)
        self.make_classes_01(o1y)
        self.make_classes_01(o2y)

        return (o1x, o1y, o2x, o2y)

    def print_generated_pairs(self):
        for o1, o2 in zip(self.drawn_objects1, self.drawn_objects2):
            print('obiekt_1 [{}] <==> obiekt_2 [{}]'.format(o1, o2))

    def get_empirical_matrix(self):
        # zmienna n24 oznacza liczbę par takich, że obiekt_1 ma klasę 2 i obiekt_2 ma klasę 4 itd.
        n22, n24, n42, n44 = 0, 0, 0, 0
        for o1, o2 in zip(self.drawn_objects1, self.drawn_objects2):
            if o1[self.class_index] == self.states[0] and o2[self.class_index] == self.states[0]:
                n22 += 1
            elif o1[self.class_index] == self.states[0] and o2[self.class_index] == self.states[1]:
                n24 += 1
            elif o1[self.class_index] == self.states[1] and o2[self.class_index] == self.states[0]:
                n42 += 1
            else:
                n44 += 1
        empirical_matrix = np.array([[n22 / (n22 + n24), n24 / (n22 + n24)], [n42 / (n42 + n44), n44 / (n42 + n44)]])
        return np.round(empirical_matrix, 2)

    def get_trans_matrix(self):
        return np.round(self.trans_matrix, 2)

    def set_trans_matrix(self, matrix):
        self.trans_matrix = matrix

    def test(self, verbose=0):
        self.generate_samples(n_samples=2000)

        gen_matrix = self.get_trans_matrix()
        emp_matrix = self.get_empirical_matrix()

        print('macierz generująca:\n {}'.format(gen_matrix))
        print('macierz empiryczna:\n {}'.format(emp_matrix))
        if verbose:
            print(self.print_generated_pairs())

# dla testu funkcjonalności:
if __name__ == '__main__':
    gen = SamplesGenerator()
    gen.test()
